<?php
namespace Rss2Mastodon;

class ConfigService{
    
    public function saveConfig($fileName, $config)
    {
        echo "saving...";
        \yaml_emit_file($fileName, $config);
    }

    public function loadConfig($fileName)
    {
        $config = \yaml_parse_file($fileName);
        if ($config === NULL){
            throw new \Exception ('Failed to load configuration invalid yaml');
        }
        return $config;
    }
}