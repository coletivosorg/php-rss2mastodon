FROM debian:buster-slim
ENV DEBIAN_FRONTEND noninteractive

RUN apt update
RUN apt -y install composer php-curl php-zip php-cli php-mbstring curl php-xml
RUN apt -y install php-yaml
COPY entrypoint.sh /entrypoint.sh
COPY app/ /app
RUN cd /app/ && composer install

ENTRYPOINT ["/entrypoint.sh"]
